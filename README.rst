===================
xml-xpath-translate
===================

xml-xpath-translate is a tools for exctract and inject node text
using by xpath

xml-xpath-translate supports only python 3.X version.

The project code and bugtracker is hosted on 
`Bitbucket <http://bitbucket.org/RedSkotina/xml-xpath-translate/>`_. 

Thanks for downloading xml-xpath-translate !

Using:
    
    Extract:
    xml-xpath-translate [-c config] extract -i "path/to/xml" -o "path/to/po"
        Extract strings from nodes selected with xpath in root directory "path/to/xml" to gettext file  "path/to/po"
    
    Inject:    
    xml-xpath-translate [-c config] inject -i "path/to/po" -o "path/to/xml" 
        Inject strings to nodes selected with xpath in root directory "path/to/xml" from gettext file  "path/to/po"
    
    Edit element xpathes in config (default = xml-xpath-translate.json)
    


