import polib

#monkey patch

def pofile_make_unique(self, preserve_order = True):
    entries = self[:]
    unique_entries = []
    #we need compare only msgid
    original_hash_fn = polib.POEntry.__hash__
    polib.POEntry.__hash__ = lambda x: hash(x.msgid)
    if preserve_order:
        seen = set()
        seen_add = seen.add
        unique_entries = [ x for x in entries if not (x in seen or seen_add(x))]
    else:    
        unique_entries = set(entries)
    
    polib.POEntry.__hash__ = original_hash_fn
    self[:] = list(unique_entries)
        
polib.POFile.make_unique = pofile_make_unique


def mofile_make_unique(self, preserve_order = True):
    entries = self[:]
    unique_entries = []
    #we need compare only msgid
    original_hash_fn = polib.MOEntry.__hash__
    original_eq_fn = polib.MOEntry.__eq__
    
    polib.MOEntry.__hash__ = lambda x: hash(x.msgid)
    polib.MOEntry.__eq__ = lambda x,y: x.msgid == y.msgid
    
    if preserve_order:
        seen = set()
        seen_add = seen.add
        unique_entries = [ x for x in entries if not (x in seen or seen_add(x))]
    else:    
        unique_entries = set(entries)
    
    polib.MOEntry.__hash__ = original_hash_fn
    polib.MOEntry.__eq__ = original_eq_fn
    
    self[:] = list(unique_entries)
        
polib.MOFile.make_unique = mofile_make_unique