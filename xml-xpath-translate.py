#!/usr/bin/env python3
import polib
import polib_extended
import argparse
import json
import io
import os
import fnmatch
import lxml
from lxml import etree
import logging
import difflib

class settings:
    mode = lambda : None
    config = None
    input = None
    output = None
    version = "0.1"
    
def set_mode(mode):
    settings.mode = mode
    
def parse_args():
    parser = argparse.ArgumentParser(description='extract strings from xml nodes. version ' + settings.version)
    parser.add_argument('-c','--config', help='Config file', type=str, default=None)
    
    subparsers = parser.add_subparsers(dest='subparser_name')
    subparsers.required = True
    parser_extract = subparsers.add_parser('extract', help='extract strings')
    parser_extract.add_argument('-i','--input', help='File or directory with target files', type=str, default=None)
    parser_extract.add_argument('-o','--output', help='Output gettext file', type=str, default=None)
    parser_extract.set_defaults(func=lambda: set_mode(extract))
    parser_inject = subparsers.add_parser('inject', help='inject strings')
    parser_inject.add_argument('-i','--input', help='Input gettext file', type=str, default=None)
    parser_inject.add_argument('-o','--output', help='File or directory with target files', type=str, default=None)
    parser_inject.set_defaults(func=lambda: set_mode(inject))
    
    args = parser.parse_args()
    args.func()
    
    settings.config = parse_config(args.config if args.config else "xml-xpath-translate.json")
    
    settings.input = args.input
    settings.output = args.output
    
    if settings.input is None or settings.input is None:
        parser.error('Required --input and --output')
        
    
    
def parse_config(filename):
    config = None
    with io.open(filename, encoding = "utf-8") as f: 
        config = json.load(f)
    return config

def create_po():
    po = polib.POFile(check_for_duplicates = False)
    po.metadata = {
    'Project-Id-Version': 'xml-ex',
    'Report-Msgid-Bugs-To': 'support@zoneofgames.ru',
    'Last-Translator': 'ZOG <support@zoneofgames.ru>',
    'Language-Team': 'ZOG <support@zoneofgames.ru>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=UTF-8',
    'Content-Transfer-Encoding': '8bit',
    'Language': 'ru'
    }
    return po

def po_append(po, original_list):
    for original_str in original_list:
        entry = polib.POEntry(
            msgid = original_str,
            msgstr = u'' )
        po.append(entry) 
        
def assure_path_exists(path):
        dir = os.path.dirname(path)
        if dir != "" and not os.path.exists(dir):
                os.makedirs(dir) 
                
class FilelistFlags:
    include_empty = 0
    not_include_empty = 1

    
def filelist_from_directory(dir_path, pattern = r'*', flags = FilelistFlags.include_empty, ignore=r'', recursive = False):
    #import os
    #print(os.getcwd())
    from os.path import isfile, join, getsize
    if  recursive:
        names = [os.path.join(dp, f) for dp, dn, fn in os.walk(dir_path) for f in fn]
    else:
        names = [ f for f in os.listdir(dir_path) if isfile(join(dir_path,f)) and (flags&FilelistFlags.include_empty or getsize(join(dir_path,f))>0)]
    obscured_names = []
    for name in names:
        if not fnmatch.fnmatchcase(name, ignore):
            obscured_names.append(name)
    
    
    return fnmatch.filter(obscured_names, pattern)

import sys
def safeprint(s):
    try:
        print(s)
    except UnicodeEncodeError:
        print(s.encode('utf8').decode(sys.stdout.encoding))
        
class Localization(dict):
    debug = False
    cutoff = 0.9
    use_close_match = False
    def __init__(self, *args):
        dict.__init__(self, args)
    def __getitem__(self, key):
        if self.use_close_match:
            key = self.get_close_match(key)
        try:
            val = dict.__getitem__(self, key)
        except KeyError as e:
            if key != '':
                key2 = self.get_close_match(key)
                safeprint("WARNING: cant localize")
                if key2 == key:
                    print("Dont have translation")
                    print("requested string")
                    safeprint(key)
                    logging.debug("dont have translation for: " + key)
                else:
                    print("requested string")
                    safeprint(key)
                    print(" ".join("%02x" %int(c) for c in key.encode('utf-8')))
                    print("most closed string")
                    safeprint(key2)
                    print(" ".join("%02x" %int(c) for c in key2.encode('utf-8')))
                    logging.debug("most close translation for: " + key)
                    logging.debug(" ".join("%02x" %int(c) for c in key.encode('utf-8')))
                    logging.debug("is " + key2)
                    logging.debug(" ".join("%02x" %int(c) for c in key2.encode('utf-8')))
                
            return key
        #log.info("GET %s['%s'] = %s" % str(dict.get(self, 'name_label')), str(key), str(val)))
        #TODO: correct work with poedit
        #if key.endswith('\n') and not val.endswith('\n'):
        #    val = val + '\n'
        return val
    def __setitem__(self, key, val):
        #log.info("SET %s['%s'] = %s" % str(dict.get(self, 'name_label')), str(key), str(val)))
        dict.__setitem__(self, key, val)
    def __contains__(self, item):
        r = dict.__contains__(item)
        return r
    def get_close_match(self, key):
        if self.debug:
            safeprint ("close string for '%s'" % key)
        close_matches = difflib.get_close_matches(key, dict.keys(self), cutoff = self.cutoff)
        close_match = close_matches[0] if close_matches != [] else key
        return close_match

def parse_localization_file(filename):
    loc = Localization()
    po = polib.pofile(filename)
    for entry in po:
        loc[entry.msgid] = entry.msgstr
    #safeprint (str(po))
    return loc
    
def extract_strings_from_xml(filename):
    strings = []
    parser = etree.XMLParser(ns_clean=True)
    with io.open(filename,encoding = settings.config["encoding"]) as f:
        try:
            tree = etree.parse(f, parser)
            for target_node in settings.config["target_nodes"]:
                nodes = tree.xpath(target_node)
                for node in nodes:
                    if node.text is not None:
                        strings.append(node.text)
        except lxml.etree.XMLSyntaxError as ex:
            safeprint(ex)
    return strings

def inject_strings_to_xml(filename, localization):
    strings = []
    tree = None
    parser = etree.XMLParser(ns_clean=True)
    with io.open(filename, encoding = settings.config["encoding"]) as f:
        try:
            tree = etree.parse(f, parser)
            for target_node in settings.config["target_nodes"]:
                nodes = tree.xpath(target_node)
                for node in nodes:
                    if node.text is not None:
                        loc_str = localization[node.text]
                        node.text = loc_str if loc_str != "" else node.text
        except lxml.etree.XMLSyntaxError as ex:
            safeprint(ex)
            return strings
            
    tree.write(filename, encoding = settings.config["encoding"], pretty_print=True, xml_declaration=True )
    
    return strings
    
def extract():
    print ("extract")
    files = []
    if os.path.isfile(settings.input):
        files = [settings.input]
    else:
        files = filelist_from_directory(settings.input, "*.xml", recursive = True)
    
    po = create_po()
    for file in files:
        print (file)
        strings = extract_strings_from_xml(file)
        po_append(po,strings)
    
    po.make_unique()
    assure_path_exists(settings.output)
    po.save(settings.output)
    
def inject():
    print ("inject")
    files = []
    if os.path.isfile(settings.output):
        files = [settings.output]
    else:
        files = filelist_from_directory(settings.output, "*.xml", recursive = True)
    
    loc = parse_localization_file(settings.input)
    
    for file in files:
        print (file)
        inject_strings_to_xml(file, loc)
    

if __name__ == "__main__":
    parse_args()
    settings.mode()

